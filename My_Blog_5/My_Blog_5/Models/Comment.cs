﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace My_Blog_5.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required]
        [StringLength(250, MinimumLength = 5)]
        public String Body { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
       
        [Required]
        [StringLength(100, MinimumLength = 5)]
        public String Author { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}