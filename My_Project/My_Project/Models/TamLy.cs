﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace My_Project.Models
{
    public class TamLy
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(100, ErrorMessage = "Số ký tự tối thiểu 5", MinimumLength = 5)]
        public string Title { set; get; }

        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(1000, ErrorMessage = "Số ký tự tối thiểu 20", MinimumLength = 20)]
        public string Body { set; get; }

        [Required(ErrorMessage = "Không được để trống")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }


        public int UserProfileUserID { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}