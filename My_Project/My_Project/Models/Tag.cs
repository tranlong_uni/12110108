﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace My_Project.Models
{
    public class Tag
    {
        public int TagID { set; get; }

        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(100, ErrorMessage = "Số ký tự trong khoảng 5-100", MinimumLength = 5)]
        public string Content { set; get; }

        public virtual ICollection<SucKhoe> SucKhoes { set; get; }
        public virtual ICollection<TinhCam> TinhCams { set; get; }
        public virtual ICollection<GiaDinh> GiaDinhs { set; get; }
        public virtual ICollection<TamLy> TamLies { set; get; }
        public virtual ICollection<HocTap> HocTaps { set; get; }
        public virtual ICollection<CongViec> CongViecs { set; get; }
        public virtual ICollection<XaHoi> XaHois { set; get; }
    }
}