﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace My_Project.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(1000, ErrorMessage = "Số ký tự tối thiểu 5", MinimumLength = 5)]
        public String Body { set; get; }

        [Required(ErrorMessage = "Không được để trống")]
        public DateTime DateCreated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(100, ErrorMessage = "Số ký tự tối thiểu 1", MinimumLength = 1)]
        public string Author { set; get; }

        public int SucKhoeID { set; get; }
        public virtual SucKhoe SucKhoe { set; get; }
        public int TinhCamID { set; get; }
        public virtual TinhCam TinhCam { set; get; }
        public int GiaDinhID { set; get; }
        public virtual GiaDinh GiaDinh { set; get; }
        public int TamLyID { set; get; }
        public virtual TamLy TamLy { set; get; }
        public int HocTapID { set; get; }
        public virtual HocTap HocTap { set; get; }
        public int CongViecID { set; get; }
        public virtual CongViec CongViec { set; get; }
        public int XaHoiID { set; get; }
        public virtual XaHoi XaHoi { set; get; }
    }
}