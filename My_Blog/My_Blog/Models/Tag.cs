﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace My_Blog.Models
{
    public class Tag
    {
        public int TagID { set; get; }

        [Required]
        [StringLength(100, MinimumLength = 10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}