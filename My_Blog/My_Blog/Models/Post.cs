﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace My_Blog.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required]
        [StringLength(500, MinimumLength = 20)]
        public String Title { set; get; }

        [Required]
        [StringLength(250, MinimumLength = 50)]
        public String Body { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }

        public int AccountID { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

        public virtual Account Account { set; get; }
        
       
    }
}