﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace My_Blog.Models
{
    public class Account
    {
        public int ID { set; get; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        public String LastName { set; get; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        public String FirstName { set; get; }

        [Required]
        [DataType(DataType.Password)]
        public String Password { set; get; }

        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}