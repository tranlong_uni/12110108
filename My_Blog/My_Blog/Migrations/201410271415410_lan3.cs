namespace My_Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.KetNoiPosts", "KetNoi_ID", "dbo.KetNois");
            DropForeignKey("dbo.KetNoiPosts", "Post_ID", "dbo.Posts");
            DropIndex("dbo.KetNoiPosts", new[] { "KetNoi_ID" });
            DropIndex("dbo.KetNoiPosts", new[] { "Post_ID" });
            CreateTable(
                "dbo.KetNoi_Post",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        KetNoiID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.KetNoiID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.KetNois", t => t.KetNoiID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.KetNoiID);
            
            DropTable("dbo.KetNoiPosts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.KetNoiPosts",
                c => new
                    {
                        KetNoi_ID = c.Int(nullable: false),
                        Post_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.KetNoi_ID, t.Post_ID });
            
            DropIndex("dbo.KetNoi_Post", new[] { "KetNoiID" });
            DropIndex("dbo.KetNoi_Post", new[] { "PostID" });
            DropForeignKey("dbo.KetNoi_Post", "KetNoiID", "dbo.KetNois");
            DropForeignKey("dbo.KetNoi_Post", "PostID", "dbo.Posts");
            DropTable("dbo.KetNoi_Post");
            CreateIndex("dbo.KetNoiPosts", "Post_ID");
            CreateIndex("dbo.KetNoiPosts", "KetNoi_ID");
            AddForeignKey("dbo.KetNoiPosts", "Post_ID", "dbo.Posts", "ID", cascadeDelete: true);
            AddForeignKey("dbo.KetNoiPosts", "KetNoi_ID", "dbo.KetNois", "ID", cascadeDelete: true);
        }
    }
}
