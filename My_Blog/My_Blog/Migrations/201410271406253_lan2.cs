namespace My_Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SucKhoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LastName = c.String(),
                        FirstName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.KetNois",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        Author = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.KetNoiPosts",
                c => new
                    {
                        KetNoi_ID = c.Int(nullable: false),
                        Post_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.KetNoi_ID, t.Post_ID })
                .ForeignKey("dbo.KetNois", t => t.KetNoi_ID, cascadeDelete: true)
                .ForeignKey("dbo.Posts", t => t.Post_ID, cascadeDelete: true)
                .Index(t => t.KetNoi_ID)
                .Index(t => t.Post_ID);
            
            AddColumn("dbo.Posts", "AccountID", c => c.Int(nullable: false));
            AddColumn("dbo.Comments", "SucKhoeID", c => c.Int(nullable: false));
            AddForeignKey("dbo.Posts", "AccountID", "dbo.Accounts", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Comments", "SucKhoeID", "dbo.SucKhoes", "ID", cascadeDelete: true);
            CreateIndex("dbo.Posts", "AccountID");
            CreateIndex("dbo.Comments", "SucKhoeID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.KetNoiPosts", new[] { "Post_ID" });
            DropIndex("dbo.KetNoiPosts", new[] { "KetNoi_ID" });
            DropIndex("dbo.Comments", new[] { "SucKhoeID" });
            DropIndex("dbo.Posts", new[] { "AccountID" });
            DropForeignKey("dbo.KetNoiPosts", "Post_ID", "dbo.Posts");
            DropForeignKey("dbo.KetNoiPosts", "KetNoi_ID", "dbo.KetNois");
            DropForeignKey("dbo.Comments", "SucKhoeID", "dbo.SucKhoes");
            DropForeignKey("dbo.Posts", "AccountID", "dbo.Accounts");
            DropColumn("dbo.Comments", "SucKhoeID");
            DropColumn("dbo.Posts", "AccountID");
            DropTable("dbo.KetNoiPosts");
            DropTable("dbo.KetNois");
            DropTable("dbo.Accounts");
            DropTable("dbo.SucKhoes");
        }
    }
}
