﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using My_Project.Models;

namespace My_Project.Controllers
{
    [Authorize]
    public class XaHoiController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /XaHoi/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var xahois = db.XaHois.Include(x => x.UserProfile);
            return View(xahois.ToList());
        }

        //
        // GET: /XaHoi/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            XaHoi xahoi = db.XaHois.Find(id);
            ViewData["idpost"] = id;
            if (xahoi == null)
            {
                return HttpNotFound();
            }
            return View(xahoi);
        }

        //
        // GET: /XaHoi/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /XaHoi/Create

        [HttpPost]
        public ActionResult Create(XaHoi xahoi, string Content)
        {
            if (ModelState.IsValid)
            {
                xahoi.DateCreated = DateTime.Now;

                //tao list cac tag
                List<Tag> Tags = new List<Tag>();
                //tach cac tag theo dau ,
                string[] TagContent = Content.Split(',');
                //lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //tim xem tag content da co hay chua
                    Tag tagExits = null;

                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //neu co tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.XaHois.Add(xahoi);
                    }
                    else
                    {
                        //neu chua co tag thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.XaHois = new List<XaHoi>();
                        tagExits.XaHois.Add(xahoi);
                    }
                    // add vao lish cac tag
                    Tags.Add(tagExits);
                }
                //gan List cac tag cho post
                xahoi.Tags = Tags;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                xahoi.UserProfileUserID = userid;

                db.XaHois.Add(xahoi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", xahoi.UserProfileUserID);
            return View(xahoi);
        }

        //
        // GET: /XaHoi/Edit/5

        public ActionResult Edit(int id = 0)
        {
            XaHoi xahoi = db.XaHois.Find(id);
            if (xahoi == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", xahoi.UserProfileUserID);
            return View(xahoi);
        }

        //
        // POST: /XaHoi/Edit/5

        [HttpPost]
        public ActionResult Edit(XaHoi xahoi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(xahoi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", xahoi.UserProfileUserID);
            return View(xahoi);
        }

        //
        // GET: /XaHoi/Delete/5

        public ActionResult Delete(int id = 0)
        {
            XaHoi xahoi = db.XaHois.Find(id);
            if (xahoi == null)
            {
                return HttpNotFound();
            }
            return View(xahoi);
        }

        //
        // POST: /XaHoi/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            XaHoi xahoi = db.XaHois.Find(id);
            db.XaHois.Remove(xahoi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}