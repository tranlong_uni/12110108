﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using My_Project.Models;

namespace My_Project.Controllers
{
    [Authorize]
    public class TamLyController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /TamLy/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var tamlies = db.TamLies.Include(t => t.UserProfile);
            return View(tamlies.ToList());
        }

        //
        // GET: /TamLy/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            TamLy tamly = db.TamLies.Find(id);
            ViewData["idpost"] = id;
            if (tamly == null)
            {
                return HttpNotFound();
            }
            return View(tamly);
        }

        //
        // GET: /TamLy/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /TamLy/Create

        [HttpPost]
        public ActionResult Create(TamLy tamly, string Content)
        {
            if (ModelState.IsValid)
            {
                tamly.DateCreated = DateTime.Now;

                //tao list cac tag
                List<Tag> Tags = new List<Tag>();
                //tach cac tag theo dau ,
                string[] TagContent = Content.Split(',');
                //lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //tim xem tag content da co hay chua
                    Tag tagExits = null;

                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //neu co tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.TamLies.Add(tamly);
                    }
                    else
                    {
                        //neu chua co tag thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.TamLies = new List<TamLy>();
                        tagExits.TamLies.Add(tamly);
                    }
                    // add vao lish cac tag
                    Tags.Add(tagExits);
                }
                //gan List cac tag cho post
                tamly.Tags = Tags;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                tamly.UserProfileUserID = userid;

                db.TamLies.Add(tamly);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", tamly.UserProfileUserID);
            return View(tamly);
        }

        //
        // GET: /TamLy/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TamLy tamly = db.TamLies.Find(id);
            if (tamly == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", tamly.UserProfileUserID);
            return View(tamly);
        }

        //
        // POST: /TamLy/Edit/5

        [HttpPost]
        public ActionResult Edit(TamLy tamly)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tamly).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", tamly.UserProfileUserID);
            return View(tamly);
        }

        //
        // GET: /TamLy/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TamLy tamly = db.TamLies.Find(id);
            if (tamly == null)
            {
                return HttpNotFound();
            }
            return View(tamly);
        }

        //
        // POST: /TamLy/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TamLy tamly = db.TamLies.Find(id);
            db.TamLies.Remove(tamly);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}