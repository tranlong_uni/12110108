﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using My_Project.Models;

namespace My_Project.Controllers
{
    [Authorize]
    public class GiaDinhController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /GiaDinh/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var giadinhs = db.GiaDinhs.Include(g => g.UserProfile);
            return View(giadinhs.ToList());
        }

        //
        // GET: /GiaDinh/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            GiaDinh giadinh = db.GiaDinhs.Find(id);
            ViewData["idpost"] = id;
            if (giadinh == null)
            {
                return HttpNotFound();
            }
            return View(giadinh);
        }

        //
        // GET: /GiaDinh/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /GiaDinh/Create

        [HttpPost]
        public ActionResult Create(GiaDinh giadinh, string Content)
        {
            if (ModelState.IsValid)
            {
                giadinh.DateCreated = DateTime.Now;

                //tao list cac tag
                List<Tag> Tags = new List<Tag>();
                //tach cac tag theo dau ,
                string[] TagContent = Content.Split(',');
                //lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //tim xem tag content da co hay chua
                    Tag tagExits = null;

                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //neu co tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.GiaDinhs.Add(giadinh);
                    }
                    else
                    {
                        //neu chua co tag thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.GiaDinhs = new List<GiaDinh>();
                        tagExits.GiaDinhs.Add(giadinh);
                    }
                    // add vao lish cac tag
                    Tags.Add(tagExits);
                }
                //gan List cac tag cho post
                giadinh.Tags = Tags;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                giadinh.UserProfileUserID = userid;

                db.GiaDinhs.Add(giadinh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", giadinh.UserProfileUserID);
            return View(giadinh);
        }

        //
        // GET: /GiaDinh/Edit/5

        public ActionResult Edit(int id = 0)
        {
            GiaDinh giadinh = db.GiaDinhs.Find(id);
            if (giadinh == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", giadinh.UserProfileUserID);
            return View(giadinh);
        }

        //
        // POST: /GiaDinh/Edit/5

        [HttpPost]
        public ActionResult Edit(GiaDinh giadinh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(giadinh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", giadinh.UserProfileUserID);
            return View(giadinh);
        }

        //
        // GET: /GiaDinh/Delete/5

        public ActionResult Delete(int id = 0)
        {
            GiaDinh giadinh = db.GiaDinhs.Find(id);
            if (giadinh == null)
            {
                return HttpNotFound();
            }
            return View(giadinh);
        }

        //
        // POST: /GiaDinh/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            GiaDinh giadinh = db.GiaDinhs.Find(id);
            db.GiaDinhs.Remove(giadinh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}