﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using My_Project.Models;

namespace My_Project.Controllers
{
    [Authorize]
    public class CongViecController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /CongViec/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var congviecs = db.CongViecs.Include(c => c.UserProfile);
            return View(congviecs.ToList());
        }

        //
        // GET: /CongViec/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            CongViec congviec = db.CongViecs.Find(id);
            ViewData["idpost"] = id;
            if (congviec == null)
            {
                return HttpNotFound();
            }
            return View(congviec);
        }

        //
        // GET: /CongViec/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /CongViec/Create

        [HttpPost]
        public ActionResult Create(CongViec congviec, string Content)
        {
            if (ModelState.IsValid)
            {
                congviec.DateCreated = DateTime.Now;

                //tao list cac tag
                List<Tag> Tags = new List<Tag>();
                //tach cac tag theo dau ,
                string[] TagContent = Content.Split(',');
                //lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //tim xem tag content da co hay chua
                    Tag tagExits = null;

                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //neu co tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.CongViecs.Add(congviec);
                    }
                    else
                    {
                        //neu chua co tag thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.CongViecs = new List<CongViec>();
                        tagExits.CongViecs.Add(congviec);
                    }
                    // add vao lish cac tag
                    Tags.Add(tagExits);
                }
                //gan List cac tag cho post
                congviec.Tags = Tags;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                congviec.UserProfileUserID = userid;

                db.CongViecs.Add(congviec);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", congviec.UserProfileUserID);
            return View(congviec);
        }

        //
        // GET: /CongViec/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CongViec congviec = db.CongViecs.Find(id);
            if (congviec == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", congviec.UserProfileUserID);
            return View(congviec);
        }

        //
        // POST: /CongViec/Edit/5

        [HttpPost]
        public ActionResult Edit(CongViec congviec)
        {
            if (ModelState.IsValid)
            {
                db.Entry(congviec).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", congviec.UserProfileUserID);
            return View(congviec);
        }

        //
        // GET: /CongViec/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CongViec congviec = db.CongViecs.Find(id);
            if (congviec == null)
            {
                return HttpNotFound();
            }
            return View(congviec);
        }

        //
        // POST: /CongViec/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            CongViec congviec = db.CongViecs.Find(id);
            db.CongViecs.Remove(congviec);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}