﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using My_Project.Models;

namespace My_Project.Controllers
{
    [Authorize]
    public class TinhCamController : Controller
    {
        
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /TinhCam/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var tinhcams = db.TinhCams.Include(t => t.UserProfile);
            return View(tinhcams.ToList());
        }

        //
        // GET: /TinhCam/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            TinhCam tinhcam = db.TinhCams.Find(id);
            ViewData["idpost"] = id;
            if (tinhcam == null)
            {
                return HttpNotFound();
            }
            return View(tinhcam);
        }

        //
        // GET: /TinhCam/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /TinhCam/Create

        [HttpPost]
        public ActionResult Create(TinhCam tinhcam, string Content)
        {
            if (ModelState.IsValid)
            {
                tinhcam.DateCreated = DateTime.Now;

                //tao list cac tag
                List<Tag> Tags = new List<Tag>();
                //tach cac tag theo dau ,
                string[] TagContent = Content.Split(',');
                //lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //tim xem tag content da co hay chua
                    Tag tagExits = null;

                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //neu co tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.TinhCams.Add(tinhcam);
                    }
                    else
                    {
                        //neu chua co tag thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.TinhCams = new List<TinhCam>();
                        tagExits.TinhCams.Add(tinhcam);
                    }
                    // add vao lish cac tag
                    Tags.Add(tagExits);
                }
                //gan List cac tag cho post
                tinhcam.Tags = Tags;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                tinhcam.UserProfileUserID = userid;

                db.TinhCams.Add(tinhcam);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", tinhcam.UserProfileUserID);
            return View(tinhcam);
        }

        //
        // GET: /TinhCam/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TinhCam tinhcam = db.TinhCams.Find(id);
            if (tinhcam == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", tinhcam.UserProfileUserID);
            return View(tinhcam);
        }

        //
        // POST: /TinhCam/Edit/5

        [HttpPost]
        public ActionResult Edit(TinhCam tinhcam)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tinhcam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", tinhcam.UserProfileUserID);
            return View(tinhcam);
        }

        //
        // GET: /TinhCam/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TinhCam tinhcam = db.TinhCams.Find(id);
            if (tinhcam == null)
            {
                return HttpNotFound();
            }
            return View(tinhcam);
        }

        //
        // POST: /TinhCam/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TinhCam tinhcam = db.TinhCams.Find(id);
            db.TinhCams.Remove(tinhcam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}