﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using My_Project.Models;

namespace My_Project.Controllers
{
    public class CommentController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Comment/

        public ActionResult Index()
        {
            var comments = db.Comments.Include(c => c.SucKhoe).Include(c => c.TinhCam).Include(c => c.GiaDinh).Include(c => c.TamLy).Include(c => c.HocTap).Include(c => c.CongViec).Include(c => c.XaHoi);
            return View(comments.ToList());
        }

        //
        // GET: /Comment/Details/5

        public ActionResult Details(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // GET: /Comment/Create

        public ActionResult Create()
        {
            ViewBag.SucKhoeID = new SelectList(db.SucKhoes, "ID", "Title");
            ViewBag.TinhCamID = new SelectList(db.TinhCams, "ID", "Title");
            ViewBag.GiaDinhID = new SelectList(db.GiaDinhs, "ID", "Title");
            ViewBag.TamLyID = new SelectList(db.TamLies, "ID", "Title");
            ViewBag.HocTapID = new SelectList(db.HocTaps, "ID", "Title");
            ViewBag.CongViecID = new SelectList(db.CongViecs, "ID", "Title");
            ViewBag.XaHoiID = new SelectList(db.XaHois, "ID", "Title");
            return View();
        }

        //
        // POST: /Comment/Create

        [HttpPost]
        public ActionResult Create(Comment comment, int idpost)
        {
            if (ModelState.IsValid)
            {
                comment.SucKhoeID = idpost;
                comment.TinhCamID = idpost;
                comment.GiaDinhID = idpost;
                comment.TamLyID = idpost;
                comment.HocTapID = idpost;
                comment.CongViecID = idpost;
                comment.XaHoiID = idpost;

                comment.DateCreated = DateTime.Now;
                db.Comments.Add(comment);
                db.SaveChanges();
                return PartialView("_viewComment", comment);
            }

            ViewBag.SucKhoeID = new SelectList(db.SucKhoes, "ID", "Title", comment.SucKhoeID);
            ViewBag.TinhCamID = new SelectList(db.TinhCams, "ID", "Title", comment.TinhCamID);
            ViewBag.GiaDinhID = new SelectList(db.GiaDinhs, "ID", "Title", comment.GiaDinhID);
            ViewBag.TamLyID = new SelectList(db.TamLies, "ID", "Title", comment.TamLyID);
            ViewBag.HocTapID = new SelectList(db.HocTaps, "ID", "Title", comment.HocTapID);
            ViewBag.CongViecID = new SelectList(db.CongViecs, "ID", "Title", comment.CongViecID);
            ViewBag.XaHoiID = new SelectList(db.XaHois, "ID", "Title", comment.XaHoiID);
            return View(comment);
        }

        //
        // GET: /Comment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.SucKhoeID = new SelectList(db.SucKhoes, "ID", "Title", comment.SucKhoeID);
            ViewBag.TinhCamID = new SelectList(db.TinhCams, "ID", "Title", comment.TinhCamID);
            ViewBag.GiaDinhID = new SelectList(db.GiaDinhs, "ID", "Title", comment.GiaDinhID);
            ViewBag.TamLyID = new SelectList(db.TamLies, "ID", "Title", comment.TamLyID);
            ViewBag.HocTapID = new SelectList(db.HocTaps, "ID", "Title", comment.HocTapID);
            ViewBag.CongViecID = new SelectList(db.CongViecs, "ID", "Title", comment.CongViecID);
            ViewBag.XaHoiID = new SelectList(db.XaHois, "ID", "Title", comment.XaHoiID);
            return View(comment);
        }

        //
        // POST: /Comment/Edit/5

        [HttpPost]
        public ActionResult Edit(Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SucKhoeID = new SelectList(db.SucKhoes, "ID", "Title", comment.SucKhoeID);
            ViewBag.TinhCamID = new SelectList(db.TinhCams, "ID", "Title", comment.TinhCamID);
            ViewBag.GiaDinhID = new SelectList(db.GiaDinhs, "ID", "Title", comment.GiaDinhID);
            ViewBag.TamLyID = new SelectList(db.TamLies, "ID", "Title", comment.TamLyID);
            ViewBag.HocTapID = new SelectList(db.HocTaps, "ID", "Title", comment.HocTapID);
            ViewBag.CongViecID = new SelectList(db.CongViecs, "ID", "Title", comment.CongViecID);
            ViewBag.XaHoiID = new SelectList(db.XaHois, "ID", "Title", comment.XaHoiID);
            return View(comment);
        }

        //
        // GET: /Comment/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /Comment/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}