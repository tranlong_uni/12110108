﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using My_Project.Models;

namespace My_Project.Controllers
{
    [Authorize]
    public class SucKhoeController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /SucKhoe/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var suckhoes = db.SucKhoes.Include(s => s.UserProfile);
            return View(suckhoes.ToList());
        }

        //
        // GET: /SucKhoe/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            SucKhoe suckhoe = db.SucKhoes.Find(id);
            ViewData["idpost"] = id;
            if (suckhoe == null)
            {
                return HttpNotFound();
            }
            return View(suckhoe);
        }

        //
        // GET: /SucKhoe/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /SucKhoe/Create

        [HttpPost]
        public ActionResult Create(SucKhoe suckhoe, string Content)
        {
            if (ModelState.IsValid)
            {
                suckhoe.DateCreated = DateTime.Now;

                //tao list cac tag
                List<Tag> Tags = new List<Tag>();
                //tach cac tag theo dau ,
                string[] TagContent = Content.Split(',');
                //lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //tim xem tag content da co hay chua
                    Tag tagExits = null;

                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //neu co tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.SucKhoes.Add(suckhoe);
                    }
                    else
                    {
                        //neu chua co tag thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.SucKhoes = new List<SucKhoe>();
                        tagExits.SucKhoes.Add(suckhoe);
                    }
                    // add vao lish cac tag
                    Tags.Add(tagExits);
                }
                //gan List cac tag cho post
                suckhoe.Tags = Tags;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                suckhoe.UserProfileUserID = userid;

                db.SucKhoes.Add(suckhoe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", suckhoe.UserProfileUserID);
            return View(suckhoe);
        }

        //
        // GET: /SucKhoe/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SucKhoe suckhoe = db.SucKhoes.Find(id);
            if (suckhoe == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", suckhoe.UserProfileUserID);
            return View(suckhoe);
        }

        //
        // POST: /SucKhoe/Edit/5

        [HttpPost]
        public ActionResult Edit(SucKhoe suckhoe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(suckhoe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", suckhoe.UserProfileUserID);
            return View(suckhoe);
        }

        //
        // GET: /SucKhoe/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SucKhoe suckhoe = db.SucKhoes.Find(id);
            if (suckhoe == null)
            {
                return HttpNotFound();
            }
            return View(suckhoe);
        }

        //
        // POST: /SucKhoe/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            SucKhoe suckhoe = db.SucKhoes.Find(id);
            db.SucKhoes.Remove(suckhoe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}