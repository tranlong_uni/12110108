﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using My_Project.Models;

namespace My_Project.Controllers
{
    [Authorize]
    public class HocTapController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /HocTap/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var hoctaps = db.HocTaps.Include(h => h.UserProfile);
            return View(hoctaps.ToList());
        }

        //
        // GET: /HocTap/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            HocTap hoctap = db.HocTaps.Find(id);
            ViewData["idpost"] = id;
            if (hoctap == null)
            {
                return HttpNotFound();
            }
            return View(hoctap);
        }

        //
        // GET: /HocTap/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /HocTap/Create

        [HttpPost]
        public ActionResult Create(HocTap hoctap, string Content)
        {
            if (ModelState.IsValid)
            {
                hoctap.DateCreated = DateTime.Now;

                //tao list cac tag
                List<Tag> Tags = new List<Tag>();
                //tach cac tag theo dau ,
                string[] TagContent = Content.Split(',');
                //lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //tim xem tag content da co hay chua
                    Tag tagExits = null;

                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //neu co tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.HocTaps.Add(hoctap);
                    }
                    else
                    {
                        //neu chua co tag thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.HocTaps = new List<HocTap>();
                        tagExits.HocTaps.Add(hoctap);
                    }
                    // add vao lish cac tag
                    Tags.Add(tagExits);
                }
                //gan List cac tag cho post
                hoctap.Tags = Tags;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                hoctap.UserProfileUserID = userid;

                db.HocTaps.Add(hoctap);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", hoctap.UserProfileUserID);
            return View(hoctap);
        }

        //
        // GET: /HocTap/Edit/5

        public ActionResult Edit(int id = 0)
        {
            HocTap hoctap = db.HocTaps.Find(id);
            if (hoctap == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", hoctap.UserProfileUserID);
            return View(hoctap);
        }

        //
        // POST: /HocTap/Edit/5

        [HttpPost]
        public ActionResult Edit(HocTap hoctap)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hoctap).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", hoctap.UserProfileUserID);
            return View(hoctap);
        }

        //
        // GET: /HocTap/Delete/5

        public ActionResult Delete(int id = 0)
        {
            HocTap hoctap = db.HocTaps.Find(id);
            if (hoctap == null)
            {
                return HttpNotFound();
            }
            return View(hoctap);
        }

        //
        // POST: /HocTap/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            HocTap hoctap = db.HocTaps.Find(id);
            db.HocTaps.Remove(hoctap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}