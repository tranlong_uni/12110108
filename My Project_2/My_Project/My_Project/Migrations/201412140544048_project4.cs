namespace My_Project.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class project4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.SucKhoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Body = c.String(nullable: false, maxLength: 1000),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false, maxLength: 1000),
                        DateCreated = c.DateTime(nullable: false),
                        Author = c.String(nullable: false, maxLength: 100),
                        SucKhoeID = c.Int(nullable: false),
                        TinhCamID = c.Int(nullable: false),
                        GiaDinhID = c.Int(nullable: false),
                        TamLyID = c.Int(nullable: false),
                        HocTapID = c.Int(nullable: false),
                        CongViecID = c.Int(nullable: false),
                        XaHoiID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SucKhoes", t => t.SucKhoeID, cascadeDelete: false)
                .ForeignKey("dbo.TinhCams", t => t.TinhCamID, cascadeDelete: false)
                .ForeignKey("dbo.GiaDinhs", t => t.GiaDinhID, cascadeDelete: false)
                .ForeignKey("dbo.TamLies", t => t.TamLyID, cascadeDelete: false)
                .ForeignKey("dbo.HocTaps", t => t.HocTapID, cascadeDelete: false)
                .ForeignKey("dbo.CongViecs", t => t.CongViecID, cascadeDelete: false)
                .ForeignKey("dbo.XaHois", t => t.XaHoiID, cascadeDelete: true)
                .Index(t => t.SucKhoeID)
                .Index(t => t.TinhCamID)
                .Index(t => t.GiaDinhID)
                .Index(t => t.TamLyID)
                .Index(t => t.HocTapID)
                .Index(t => t.CongViecID)
                .Index(t => t.XaHoiID);
            
            CreateTable(
                "dbo.TinhCams",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Body = c.String(nullable: false, maxLength: 1000),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.GiaDinhs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Body = c.String(nullable: false, maxLength: 1000),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.TamLies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Body = c.String(nullable: false, maxLength: 1000),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.HocTaps",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Body = c.String(nullable: false, maxLength: 1000),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.CongViecs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Body = c.String(nullable: false, maxLength: 1000),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.XaHois",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Body = c.String(nullable: false, maxLength: 1000),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.Tag_GiaDinh",
                c => new
                    {
                        GiaDinhID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GiaDinhID, t.TagID })
                .ForeignKey("dbo.GiaDinhs", t => t.GiaDinhID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.GiaDinhID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_TamLy",
                c => new
                    {
                        TamLyID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TamLyID, t.TagID })
                .ForeignKey("dbo.TamLies", t => t.TamLyID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.TamLyID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_HocTap",
                c => new
                    {
                        HocTapID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.HocTapID, t.TagID })
                .ForeignKey("dbo.HocTaps", t => t.HocTapID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.HocTapID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_CongViec",
                c => new
                    {
                        CongViecID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CongViecID, t.TagID })
                .ForeignKey("dbo.CongViecs", t => t.CongViecID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.CongViecID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_XaHoi",
                c => new
                    {
                        XaHoiID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.XaHoiID, t.TagID })
                .ForeignKey("dbo.XaHois", t => t.XaHoiID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.XaHoiID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_TinhCam",
                c => new
                    {
                        TinhCamID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TinhCamID, t.TagID })
                .ForeignKey("dbo.TinhCams", t => t.TinhCamID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.TinhCamID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_SucKhoe",
                c => new
                    {
                        SucKhoeID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SucKhoeID, t.TagID })
                .ForeignKey("dbo.SucKhoes", t => t.SucKhoeID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.SucKhoeID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_SucKhoe", new[] { "TagID" });
            DropIndex("dbo.Tag_SucKhoe", new[] { "SucKhoeID" });
            DropIndex("dbo.Tag_TinhCam", new[] { "TagID" });
            DropIndex("dbo.Tag_TinhCam", new[] { "TinhCamID" });
            DropIndex("dbo.Tag_XaHoi", new[] { "TagID" });
            DropIndex("dbo.Tag_XaHoi", new[] { "XaHoiID" });
            DropIndex("dbo.Tag_CongViec", new[] { "TagID" });
            DropIndex("dbo.Tag_CongViec", new[] { "CongViecID" });
            DropIndex("dbo.Tag_HocTap", new[] { "TagID" });
            DropIndex("dbo.Tag_HocTap", new[] { "HocTapID" });
            DropIndex("dbo.Tag_TamLy", new[] { "TagID" });
            DropIndex("dbo.Tag_TamLy", new[] { "TamLyID" });
            DropIndex("dbo.Tag_GiaDinh", new[] { "TagID" });
            DropIndex("dbo.Tag_GiaDinh", new[] { "GiaDinhID" });
            DropIndex("dbo.XaHois", new[] { "UserProfileUserID" });
            DropIndex("dbo.CongViecs", new[] { "UserProfileUserID" });
            DropIndex("dbo.HocTaps", new[] { "UserProfileUserID" });
            DropIndex("dbo.TamLies", new[] { "UserProfileUserID" });
            DropIndex("dbo.GiaDinhs", new[] { "UserProfileUserID" });
            DropIndex("dbo.TinhCams", new[] { "UserProfileUserID" });
            DropIndex("dbo.Comments", new[] { "XaHoiID" });
            DropIndex("dbo.Comments", new[] { "CongViecID" });
            DropIndex("dbo.Comments", new[] { "HocTapID" });
            DropIndex("dbo.Comments", new[] { "TamLyID" });
            DropIndex("dbo.Comments", new[] { "GiaDinhID" });
            DropIndex("dbo.Comments", new[] { "TinhCamID" });
            DropIndex("dbo.Comments", new[] { "SucKhoeID" });
            DropIndex("dbo.SucKhoes", new[] { "UserProfileUserID" });
            DropForeignKey("dbo.Tag_SucKhoe", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_SucKhoe", "SucKhoeID", "dbo.SucKhoes");
            DropForeignKey("dbo.Tag_TinhCam", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_TinhCam", "TinhCamID", "dbo.TinhCams");
            DropForeignKey("dbo.Tag_XaHoi", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_XaHoi", "XaHoiID", "dbo.XaHois");
            DropForeignKey("dbo.Tag_CongViec", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_CongViec", "CongViecID", "dbo.CongViecs");
            DropForeignKey("dbo.Tag_HocTap", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_HocTap", "HocTapID", "dbo.HocTaps");
            DropForeignKey("dbo.Tag_TamLy", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_TamLy", "TamLyID", "dbo.TamLies");
            DropForeignKey("dbo.Tag_GiaDinh", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_GiaDinh", "GiaDinhID", "dbo.GiaDinhs");
            DropForeignKey("dbo.XaHois", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.CongViecs", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.HocTaps", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.TamLies", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.GiaDinhs", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.TinhCams", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "XaHoiID", "dbo.XaHois");
            DropForeignKey("dbo.Comments", "CongViecID", "dbo.CongViecs");
            DropForeignKey("dbo.Comments", "HocTapID", "dbo.HocTaps");
            DropForeignKey("dbo.Comments", "TamLyID", "dbo.TamLies");
            DropForeignKey("dbo.Comments", "GiaDinhID", "dbo.GiaDinhs");
            DropForeignKey("dbo.Comments", "TinhCamID", "dbo.TinhCams");
            DropForeignKey("dbo.Comments", "SucKhoeID", "dbo.SucKhoes");
            DropForeignKey("dbo.SucKhoes", "UserProfileUserID", "dbo.UserProfile");
            DropTable("dbo.Tag_SucKhoe");
            DropTable("dbo.Tag_TinhCam");
            DropTable("dbo.Tag_XaHoi");
            DropTable("dbo.Tag_CongViec");
            DropTable("dbo.Tag_HocTap");
            DropTable("dbo.Tag_TamLy");
            DropTable("dbo.Tag_GiaDinh");
            DropTable("dbo.XaHois");
            DropTable("dbo.CongViecs");
            DropTable("dbo.HocTaps");
            DropTable("dbo.TamLies");
            DropTable("dbo.GiaDinhs");
            DropTable("dbo.Tags");
            DropTable("dbo.TinhCams");
            DropTable("dbo.Comments");
            DropTable("dbo.SucKhoes");
            DropTable("dbo.UserProfile");
        }
    }
}
