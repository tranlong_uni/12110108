﻿CREATE TABLE [dbo].[Table]
(
	[ID] INT NOT NULL PRIMARY KEY, 
    [Body] NVARCHAR(1000) NOT NULL, 
    [DateCreated] DATETIME NOT NULL, 
    [Author] NVARCHAR(100) NOT NULL, 
    [SucKhoeID] INT NOT NULL, 
    [TinhCamID] INT NOT NULL, 
    [GiaDinhID] INT NOT NULL, 
    [TamLyID] INT NOT NULL, 
    [HocTapID] INT NOT NULL, 
    [CongViecID] INT NOT NULL, 
    [XaHoiID] INT NOT NULL
)
