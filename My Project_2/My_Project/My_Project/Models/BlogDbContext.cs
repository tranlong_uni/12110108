﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace My_Project.Models
{
    public class BlogDbContext: DbContext
    {
        public BlogDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<SucKhoe> SucKhoes { get; set; }
        public DbSet<TinhCam> TinhCams { get; set; }
        public DbSet<GiaDinh> GiaDinhs { get; set; }
        public DbSet<TamLy> TamLies { get; set; }
        public DbSet<HocTap> HocTaps { get; set; }
        public DbSet<CongViec> CongViecs { get; set; }
        public DbSet<XaHoi> XaHois { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<SucKhoe>().HasMany(d => d.Tags).WithMany(p => p.SucKhoes).Map(t => t.MapLeftKey("SucKhoeID")
                .MapRightKey("TagID").ToTable("Tag_SucKhoe"));

            modelBuilder.Entity<TinhCam>().HasMany(d => d.Tags).WithMany(p => p.TinhCams).Map(t => t.MapLeftKey("TinhCamID")
                .MapRightKey("TagID").ToTable("Tag_TinhCam"));

            modelBuilder.Entity<GiaDinh>().HasMany(d => d.Tags).WithMany(p => p.GiaDinhs).Map(t => t.MapLeftKey("GiaDinhID")
                .MapRightKey("TagID").ToTable("Tag_GiaDinh"));

            modelBuilder.Entity<TamLy>().HasMany(d => d.Tags).WithMany(p => p.TamLies).Map(t => t.MapLeftKey("TamLyID")
                .MapRightKey("TagID").ToTable("Tag_TamLy"));

            modelBuilder.Entity<HocTap>().HasMany(d => d.Tags).WithMany(p => p.HocTaps).Map(t => t.MapLeftKey("HocTapID")
                .MapRightKey("TagID").ToTable("Tag_HocTap"));

            modelBuilder.Entity<CongViec>().HasMany(d => d.Tags).WithMany(p => p.CongViecs).Map(t => t.MapLeftKey("CongViecID")
                .MapRightKey("TagID").ToTable("Tag_CongViec"));

            modelBuilder.Entity<XaHoi>().HasMany(d => d.Tags).WithMany(p => p.XaHois).Map(t => t.MapLeftKey("XaHoiID")
                .MapRightKey("TagID").ToTable("Tag_XaHoi"));
        }

        public DbSet<Tag> Tags { get; set; }
    }
}