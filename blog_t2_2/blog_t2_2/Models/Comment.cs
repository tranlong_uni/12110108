﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace blog_t2_2.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public int Post_Id { set; get; }
        public String Body { get; set; }

    }
    //public class BlogDbContext : DbContext
    //{
    //    public DbSet<Comment> Comments { set; get; }
    //}
}