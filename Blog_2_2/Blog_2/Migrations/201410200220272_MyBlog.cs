namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyBlog : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false, identity: true));
        }
    }
}
